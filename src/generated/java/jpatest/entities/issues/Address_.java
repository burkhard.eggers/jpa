package jpatest.entities.issues;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Address.class)
public abstract class Address_ {

	public static volatile SingularAttribute<Address, Integer> number;
	public static volatile SingularAttribute<Address, Street> street;
	public static volatile SingularAttribute<Address, Long> id;
	public static volatile SingularAttribute<Address, User> user;

	public static final String NUMBER = "number";
	public static final String STREET = "street";
	public static final String ID = "id";
	public static final String USER = "user";

}

