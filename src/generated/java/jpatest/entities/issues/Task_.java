package jpatest.entities.issues;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Task.class)
public abstract class Task_ extends jpatest.entities.issues.TaskContainer_ {

	public static volatile SingularAttribute<Task, Long> resolvedByUser;
	public static volatile SingularAttribute<Task, Issue> issue;
	public static volatile SingularAttribute<Task, Long> id;

	public static final String RESOLVED_BY_USER = "resolvedByUser";
	public static final String ISSUE = "issue";
	public static final String ID = "id";

}

