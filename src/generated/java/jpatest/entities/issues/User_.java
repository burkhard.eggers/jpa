package jpatest.entities.issues;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, String> firstName;
	public static volatile SingularAttribute<User, String> lastName;
	public static volatile ListAttribute<User, Issue> assignments;
	public static volatile SingularAttribute<User, Address> address;
	public static volatile SingularAttribute<User, Long> id;

	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String ASSIGNMENTS = "assignments";
	public static final String ADDRESS = "address";
	public static final String ID = "id";

}

