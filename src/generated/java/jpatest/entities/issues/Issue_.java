package jpatest.entities.issues;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Issue.class)
public abstract class Issue_ extends jpatest.entities.issues.TaskContainer_ {

	public static volatile SingularAttribute<Issue, IssueSeverity> severity;
	public static volatile SingularAttribute<Issue, Long> id;
	public static volatile SingularAttribute<Issue, String> content;
	public static volatile ListAttribute<Issue, User> userAssignments;
	public static volatile SingularAttribute<Issue, Status> status;

	public static final String SEVERITY = "severity";
	public static final String ID = "id";
	public static final String CONTENT = "content";
	public static final String USER_ASSIGNMENTS = "userAssignments";
	public static final String STATUS = "status";

}

