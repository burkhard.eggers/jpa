package jpatest.entities.issues;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Street.class)
public abstract class Street_ {

	public static volatile SingularAttribute<Street, City> city;
	public static volatile SingularAttribute<Street, String> name;
	public static volatile SingularAttribute<Street, Long> id;

	public static final String CITY = "city";
	public static final String NAME = "name";
	public static final String ID = "id";

}

