package jpatest.entities.issues;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaskContainer.class)
public abstract class TaskContainer_ {

	public static volatile SingularAttribute<TaskContainer, TaskContainer> container;
	public static volatile ListAttribute<TaskContainer, Task> subTasks;
	public static volatile SingularAttribute<TaskContainer, Long> id;
	public static volatile SingularAttribute<TaskContainer, String> title;

	public static final String CONTAINER = "container";
	public static final String SUB_TASKS = "subTasks";
	public static final String ID = "id";
	public static final String TITLE = "title";

}

