package jpatest.entities.embeddedId;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Incident.class)
public abstract class Incident_ {

	public static volatile SingularAttribute<Incident, String> what;
	public static volatile SingularAttribute<Incident, IncidentId> id;

	public static final String WHAT = "what";
	public static final String ID = "id";

}

