package jpatest.entities.embeddedId;

import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IncidentId.class)
public abstract class IncidentId_ {

	public static volatile SingularAttribute<IncidentId, LocalDate> date;
	public static volatile SingularAttribute<IncidentId, IncidentType> type;

	public static final String DATE = "date";
	public static final String TYPE = "type";

}

