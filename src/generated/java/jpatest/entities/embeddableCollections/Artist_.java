package jpatest.entities.embeddableCollections;

import java.time.LocalDate;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Artist.class)
public abstract class Artist_ {

	public static volatile SingularAttribute<Artist, String> firstName;
	public static volatile SingularAttribute<Artist, String> lastName;
	public static volatile SingularAttribute<Artist, LocalDate> dateOfBirth;
	public static volatile SingularAttribute<Artist, Long> id;
	public static volatile SingularAttribute<Artist, String> email;
	public static volatile ListAttribute<Artist, Track> tracks;
	public static volatile ListAttribute<Artist, String> tags;

	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String ID = "id";
	public static final String EMAIL = "email";
	public static final String TRACKS = "tracks";
	public static final String TAGS = "tags";

}

