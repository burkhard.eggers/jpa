package jpatest.entities.embeddableCollections;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Track.class)
public abstract class Track_ {

	public static volatile SingularAttribute<Track, Integer> duration;
	public static volatile SingularAttribute<Track, String> name;

	public static final String DURATION = "duration";
	public static final String NAME = "name";

}

