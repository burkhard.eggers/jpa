package jpatest.entities.oneToMany;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Many.class)
public abstract class Many_ {

	public static volatile SingularAttribute<Many, One> one;
	public static volatile SingularAttribute<Many, String> name;
	public static volatile SingularAttribute<Many, Long> id;

	public static final String ONE = "one";
	public static final String NAME = "name";
	public static final String ID = "id";

}

