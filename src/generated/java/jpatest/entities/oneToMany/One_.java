package jpatest.entities.oneToMany;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(One.class)
public abstract class One_ {

	public static volatile SingularAttribute<One, String> name;
	public static volatile SingularAttribute<One, Long> id;
	public static volatile ListAttribute<One, Many> many;

	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String MANY = "many";

}

