package jpatest.entities.manyToMany;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Left.class)
public abstract class Left_ {

	public static volatile ListAttribute<Left, Right> rights;
	public static volatile SingularAttribute<Left, String> name;
	public static volatile SingularAttribute<Left, Long> id;

	public static final String RIGHTS = "rights";
	public static final String NAME = "name";
	public static final String ID = "id";

}

