package jpatest.entities.manyToMany;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Right.class)
public abstract class Right_ {

	public static volatile SingularAttribute<Right, String> name;
	public static volatile SingularAttribute<Right, Long> id;
	public static volatile ListAttribute<Right, Left> lefts;

	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String LEFTS = "lefts";

}

