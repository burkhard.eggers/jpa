package issues;

import jpatest.entities.issues.Street;;

public class NrUsersPerStreet {

    public NrUsersPerStreet(Street street, long count){
        this.street = street;
        this.count = count;
    }

    private Street street;

    private long count;

    public Street getStreet(){
        return street;
    }

    public long getCount() {
        return count;
    }

    public String toString(){
        return street.getName() + " : " + count;
    }
}
