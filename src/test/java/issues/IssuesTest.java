package issues;

import entities.BaseTest;
import jpatest.entities.issues.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IssuesTest extends BaseTest {

    private Issue i1, i2;
    private User u1, u2;
    private Task t1, t11;
    private City koln, aachen;
    private Street strk, stra, strk2;

    @Override
    protected void prepareEach() {
        tx.begin();
        em.createQuery("delete from Issue").executeUpdate();

        // issues
        i1 = new Issue("funktioniert überhaupt nicht!", IssueSeverity.HIGH);
        i2 = new Issue("kleiner fehler!", IssueSeverity.LOW);

        // cities
        koln = new City("Köln");
        aachen = new City("Aachen");

        // Streets
        strk = new Street("Domplatte");
        strk2 = new Street("kölner straße");
        stra = new Street("Aachener Straße");
        koln.addStreet(strk).addStreet(strk2);
        aachen.addStreet(stra);

        // users
        u1 = new User("Huy", "Do");
        u1.setAddress(new Address().setStreet(strk).setNumber(2));
        u2 = new User("Joschua", "Grube");
        u2.setAddress(new Address().setStreet(stra).setNumber(4));
        User u3 = new User("Bin", "Kölner").setAddress(new Address().setStreet(strk2).setNumber(3));
        User woelki = new User("Der", "Woelki").setAddress(new Address().setStreet(strk).setNumber(5));


        u1.assign(i1).assign(i2);
        u2.assign(i1);

        t1 = new Task("Startet nicht!");
        t11 = new Task("ist Schrott");
        t1.addSubTask(t11);
        i1.addSubTask(t1);

        em.persist(koln);
        em.persist(aachen);
        em.persist(i1);
        em.persist(i2);
        em.persist(u3);
        em.persist(woelki);
        tx.commit();
    }

    @Test
    @DisplayName("check preparation")
    void checkPreparation(){
        // should be two issues
        assertEquals(2, em.createQuery("select i from Issue i").getResultList().size());

        // retrieve issue 1
        Issue issue1Retr = em.find(Issue.class, i1.getId());
        assertEquals(i1, issue1Retr);

        // retrieve issue 2
        Issue issue2Retr = em.find(Issue.class, i2.getId());
        assertEquals(i2, issue2Retr);


        // i1 should be assigned to u1 and u2
        assertEquals(2, issue1Retr.getUserAssignments().size());
        assertEquals(true, issue1Retr.getUserAssignments().contains(u1));
        assertEquals(true, issue1Retr.getUserAssignments().contains(u2));

        // i2 should be assigned to u1 only
        assertEquals(1, issue2Retr.getUserAssignments().size());
        assertEquals(true, issue2Retr.getUserAssignments().contains(u1));
        assertEquals(false, issue2Retr.getUserAssignments().contains(u2));

        //i2 should have no task
        assertEquals(0, issue2Retr.getSubTasks().size());

        //i1 should have single task with a single subtask
        assertEquals(1, issue1Retr.getSubTasks().get(0).getSubTasks().size());
        // that subtask's parent task should be the task of i1
        assertEquals(t1, issue1Retr.getSubTasks().get(0).getSubTasks().get(0).getContainer());

        // all subtasks of i1 should have i1 as issue
        taskAndItsSubTasksShouldPointToIssue(issue1Retr.getSubTasks().get(0), issue1Retr);
    }

    @Test
    @DisplayName("check DAO")
    void checkDAO(){
        IssuesDAO dao = new IssuesDAO(em);

        // there should be two issue that are not resolved but assigned
        List<Issue> issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.ASSIGNED);
        assertEquals(List.of(i1, i2), issuesOfU1);
        issuesOfU1.forEach((i) -> assertEquals(Status.ASSIGNED, i.getStatus()));

        // there should be two unresolved tasks for user u1
        List<Task> tasks = dao.unResolvedTasksForUsersIssues(u1.getId());
        assertEquals(2, tasks.size());

        // unassign them
        issuesOfU1.forEach((i) -> {
            dao.unassign(u1.getId(), i.getId());
        });

        // should be no one assigned now
        issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.ASSIGNED);
        assertEquals(0, issuesOfU1.size());

        // no unresolved task should be part of u1's issues
        tasks = dao.unResolvedTasksForUsersIssues(u1.getId());
        assertEquals(0, tasks.size());

        // assign them again
        List.of(i1, i2).forEach((i) -> {
            dao.assign(u1.getId(), i.getId());
        });

        // should be two assigned
        issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.ASSIGNED);
        assertEquals(2, issuesOfU1.size());

        // resolve all tasks of i1
        dao.resolveTask(u1.getId(), t1.getId());
        dao.resolveTask(u1.getId(), t11.getId());

        issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.ASSIGNED);
        // i2 should be not resolved
        assertEquals(1, issuesOfU1.size());
        assertEquals(i2, issuesOfU1.get(0));

        // i1 should be resolved
        issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.RESOLVED);
        assertEquals(1, issuesOfU1.size());
        assertEquals(i1, issuesOfU1.get(0));

        // unresolve task t1 of i1
        dao.unresolveTask(t1.getId());

        // two issues should be assigned
        issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.ASSIGNED);
        assertEquals(2, issuesOfU1.size());

        // .. and none resolved
        issuesOfU1 = dao.issuesFromUserAndSeverity(u1.getId(), Status.RESOLVED);
        assertEquals(0, issuesOfU1.size());
    }

    @Test
    @DisplayName("street city assignment")
    void testStreetCity(){
        // street should be in köln
        assertEquals(u1.getAddress().getStreet().getCity(), koln);

        // remove it
        tx.begin();
        koln.removeStreet(strk);
        tx.commit();

        assertNull(strk.getCity());
    }

    @Test
    @DisplayName("addresses")
    void testUsersPerStreet(){
        Query query = em.createQuery(
                "select new issues.NrUsersPerStreet(a.street, count(a.street)) " +
                        "from Address a " +
                        "where a.user.firstName not like :userFirstNameExcludeMask " +
                        "group by a.street order by a.street.name");
        // all but huy
        List<NrUsersPerStreet> l = query.setParameter("userFirstNameExcludeMask", "%Huy%").getResultList();

        assertEquals(3, l.size());

        // domplatte: 1 persons
        assertEquals(strk, l.get(1).getStreet());
        assertEquals(1, l.get(1).getCount());

        // all
        l = query.setParameter("userFirstNameExcludeMask", "%xyz%").getResultList();
        assertEquals(3, l.size());


        assertEquals(3, l.size());

        // aachener one person
        assertEquals(stra, l.get(0).getStreet());
        assertEquals(1, l.get(0).getCount());

        // domplatte: 2 persons
        assertEquals(strk, l.get(1).getStreet());
        assertEquals(2, l.get(1).getCount());

        // kölner : one person
        assertEquals(strk2, l.get(2).getStreet());
        assertEquals(1, l.get(2).getCount());
    }

    void taskAndItsSubTasksShouldPointToIssue(Task t, Issue i){
        assertEquals(i, t.getIssue());
        t.getSubTasks().forEach((s) -> taskAndItsSubTasksShouldPointToIssue(s, i));
    }
}
