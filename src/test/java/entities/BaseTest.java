package entities;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public abstract class BaseTest {
    protected EntityManagerFactory emf;
    protected EntityManager em;
    protected EntityTransaction tx;

    public BaseTest(){
        emf = Persistence.createEntityManagerFactory("jpa");
    }

    @BeforeEach
    void init(){
        em = emf.createEntityManager();
        tx = em.getTransaction();
        prepareEach();
    }

    @AfterEach
    void close(){
        em.close();
        emf.close();
    }

    abstract protected void prepareEach();

}
