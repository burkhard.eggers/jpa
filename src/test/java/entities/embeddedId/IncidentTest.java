package entities.embeddedId;

import entities.BaseTest;
import jpatest.entities.embeddedId.Incident;
import jpatest.entities.embeddedId.IncidentId;
import jpatest.entities.embeddedId.IncidentType;
import org.junit.jupiter.api.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IncidentTest extends BaseTest {

    protected void prepareEach(){
        tx.begin();
        em.createQuery("delete from Incident").executeUpdate();
        tx.commit();
    }

    @Test
    @DisplayName("create and retrieve an incident")
    void createAndRetrieve(){
        Incident incident = new Incident(
                new IncidentId(IncidentType.FATAL, LocalDate.now()), "something wrong happened"
        );
        tx.begin();
        em.persist(incident);
        tx.commit();

        Incident retrieved = em.find(Incident.class, incident.getId());
        assertEquals(retrieved, incident);
    }

    @Test
    @DisplayName("insert with different and same PK")
    void insertWithDifferentPK(){
        LocalDate now = LocalDate.now();
        Incident incident = new Incident(
                new IncidentId(IncidentType.FATAL, now), "prob 1"
        );
        Incident incident2 = new Incident(
                new IncidentId(IncidentType.NORMAL, now), "prob 2"
        );

        tx.begin();
        em.persist(incident);
        em.persist(incident2);
        tx.commit();

        List list = em.createQuery("select i from Incident i").getResultList();
        assertEquals(2, list.size());
    }

    @Test
    @DisplayName("insert same PK")
    void insertSamePk(){
        LocalDate now = LocalDate.now();
        tx.begin();
        Incident incident1 = new Incident(
                new IncidentId(IncidentType.NORMAL, now),
                "prob 1"
        );
        em.persist(incident1);
        tx.commit();

        tx.begin();
        Incident incident2 = new Incident(
                new IncidentId(IncidentType.NORMAL, now),
                "prob 2"
        );
        assertThrows(EntityExistsException.class, () -> {
            tx.commit();
            em.persist(incident2);
        });

        List list = em.createQuery("select i from Incident i").getResultList();
        assertEquals(1, list.size());
    }

    void prepare3Incidents(LocalDate localDate, String normalWhat){
        tx.begin();
        Incident incident1 = new Incident(
                new IncidentId(IncidentType.NORMAL, localDate),
                normalWhat
        );
        Incident incident2 = new Incident(
                new IncidentId(IncidentType.FATAL, localDate),
                "fatal"
        );
        Incident incident3 = new Incident(
                new IncidentId(IncidentType.FATAL,
                        LocalDate.of(2022, Month.FEBRUARY, 20)
                ),
                "fatal"
        );
        em.persist(incident1);
        em.persist(incident2);
        em.persist(incident3);
        tx.commit();
    }

    @Test
    @DisplayName("retrieve per data")
    void retrievePerDate() {
        LocalDate now = LocalDate.now();
        prepare3Incidents(now, "normal");
        // must be 2 with date now
        int size = em.createQuery("select i from Incident i where i.id.date = :date")
                .setParameter("date", now)
                .getResultList()
                .size();
        assertEquals(2, size);
    }

    @Test
    @DisplayName("retrieve per type")
    void retrievePerType() {
        LocalDate now = LocalDate.now();
        prepare3Incidents(now, "normal");
        // must be two with fatal
        int size = em.createQuery("select i from Incident i where i.id.type = :type")
                .setParameter("type", IncidentType.FATAL)
                .getResultList()
                .size();
        assertEquals(2, size);
    }

    @Test
    @DisplayName("retrieve per type")
    void retrievePerTypeAndDate() {
        LocalDate now = LocalDate.now();
        String normalWhat = "jjiijoijoj";
        prepare3Incidents(now, normalWhat);

        // now and normal must be one
        List<Incident> list = em.createQuery("select i from Incident i where i.id.type = :type and i.id.date = :date")
                .setParameter("type", IncidentType.NORMAL)
                .setParameter("date", now)
                .getResultList();
        assertEquals(1, list.size());
        assertEquals(normalWhat, list.get(0).getWhat());
    }

    @Test
    @DisplayName("retrieve single id object")
    void retrievePerId() {
        LocalDate now = LocalDate.now();
        String normalWhat = "normal";
        prepare3Incidents(now, normalWhat);

        // now and normal must be one
        List<Incident> list = em.createQuery("select i from Incident i where i.id = :id")
                .setParameter("id", new IncidentId(IncidentType.NORMAL, now))
                .getResultList();
        assertEquals(1, list.size());
        assertEquals(normalWhat, list.get(0).getWhat());
    }



}
