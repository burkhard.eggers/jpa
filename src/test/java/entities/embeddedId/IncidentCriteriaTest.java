package entities.embeddedId;

import entities.BaseTest;
import jpatest.entities.embeddableCollections.Artist;
import jpatest.entities.embeddedId.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IncidentCriteriaTest extends BaseTest {
    @Override
    protected void prepareEach() {
        tx.begin();
        em.createQuery("delete from Incident").executeUpdate();
        tx.commit();
    }

    void prepare3Incidents(LocalDate localDate, String normalWhat){
        tx.begin();
        Incident incident1 = new Incident(
                new IncidentId(IncidentType.NORMAL, localDate),
                normalWhat
        );
        Incident incident2 = new Incident(
                new IncidentId(IncidentType.FATAL, localDate),
                "fatal"
        );
        Incident incident3 = new Incident(
                new IncidentId(IncidentType.FATAL,
                        LocalDate.of(2022, Month.FEBRUARY, 20)
                ),
                "fatal"
        );
        em.persist(incident1);
        em.persist(incident2);
        em.persist(incident3);
        tx.commit();
    }
    @Test
    @DisplayName("retrieve per date")
    void retrievePerDate() {
        LocalDate now = LocalDate.now();
        prepare3Incidents(now, "normal");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Incident> incidentQuery = cb.createQuery(Incident.class);
        Root<Incident> from = incidentQuery.from(Incident.class);
        List<Incident> allNormalPerDate = em.createQuery(
                        incidentQuery
                                .select(from)
                                .where(cb.equal(
                                        from
                                                .get(Incident_.id)
                                                .get(IncidentId_.date),
                                        now ))
                ).getResultList();

        // must be 2 with date now
        assertEquals(2, allNormalPerDate.size());
    }

    @Test
    @DisplayName("retrieve per date and type")
    void retrievePerDateAndType(){
        LocalDate now = LocalDate.now();
        String normalWhat = "IZIUZUIZIUZIUZ";
        prepare3Incidents(now, normalWhat);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Incident> incidentQuery = cb.createQuery(Incident.class);
        Root<Incident> from = incidentQuery.from(Incident.class);
        List<Incident> allNormalPerDate = em.createQuery(
                incidentQuery
                        .select(from)
                        .where(cb.and(
                                cb.equal(from
                                        .get(Incident_.id)
                                        .get(IncidentId_.date),
                                        now
                                ),
                                cb.equal(from
                                        .get(Incident_.id)
                                        .get(IncidentId_.type),
                                        IncidentType.NORMAL
                                )))
        ).getResultList();

        // must be 1 with date now and type normal and must have the above what
        assertEquals(1, allNormalPerDate.size());
        assertEquals(normalWhat, allNormalPerDate.get(0).getWhat());
    }



}
