package entities.oneToMany;

import entities.BaseTest;
import jpatest.entities.oneToMany.Many;
import jpatest.entities.oneToMany.One;
import jpatest.entities.oneToOne.Address;
import jpatest.entities.oneToOne.Customer;
import org.junit.jupiter.api.*;

import javax.persistence.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class OneToManyTest extends BaseTest {

    protected void prepareEach(){
        tx.begin();
        em.createQuery("delete from One").executeUpdate();
        em.createQuery("delete from Many").executeUpdate();
        tx.commit();
    }

    void prepareData(One one){
        tx.begin();
        em.persist(one);
        tx.commit();
        Many many1 = new Many("many1");
        Many many2 = new Many("many2");
        Many many3 = new Many("many3");
        one
                .addMany(many1)
                .addMany(many2)
                .addMany(many3);

        tx.begin();
        em.persist(one);
        tx.commit();
    }

    @Test
    @DisplayName("create one with many")
    void retrieveOneWithMany(){
        One one = new One("one");
        prepareData(one);
        // there should be three many instances now
        int nrMany = em.createQuery("select m from Many m").getResultList().size();
        assertEquals(3, nrMany);

        // retrieve it
        One oneRetr = em.find(One.class, one.getId());
        assertEquals(oneRetr, one);
    }

    @Test
    @DisplayName("add many")
    void addMany(){
        One one = new One("one");
        prepareData(one);
        One oneRetr = em.find(One.class, one.getId());

        // add many to it
        Many m = new Many("manynew");
        oneRetr.addMany(m);
        tx.begin();
        em.persist(oneRetr);
        tx.commit();

        // the new many should have the one
        assertEquals(one, m.getOne());

        // there should be four many now for that one
        int manySize = em.find(One.class, one.getId()).getMany().size();
        assertEquals(4, manySize);
    }

    @Test
    @DisplayName("change many")
    void changeMany(){
        One one = new One("one");
        prepareData(one);
        One oneRetr = em.find(One.class, one.getId());

        // change directly
        Many m = oneRetr.getMany().get(0);
        m.setName("new name");
        tx.begin();
        em.persist(oneRetr);
        tx.commit();

        // the many should have the one
        assertEquals(one, m.getOne());

        // there should be a many now with new name
        String name = em.find(One.class, one.getId()).getMany().get(0).getName();
        assertEquals("new name", name);

        // change in the many now
        Many many = oneRetr.getMany().get(0);
        many.setName("newer");
        em.persist(many);

        name = em.find(One.class, one.getId()).getMany().get(0).getName();
        assertEquals("newer", name);
    }

    @Test
    @DisplayName("remove many")
    void removeMany(){
        One one = new One("one");
        prepareData(one);

        // add many to it
        one.removeMany(one.getMany().get(0));
        tx.begin();
        em.persist(one);
        tx.commit();

        // there should be two many now for that one
        int manySize = em.find(One.class, one.getId()).getMany().size();
        assertEquals(2, manySize);

        // each of them should have the one assigned
        one.getMany().forEach((m) -> assertEquals(one, m.getOne()));
    }

    @Test
    @DisplayName("named query")
    void oneByManyName(){
        One one = new One("newone");
        prepareData(one);

        One one1 = (One)em.createNamedQuery("oneByManyName").setParameter("name", "many1").getSingleResult();
        assertEquals(one, one1);

        assertThrows(NoResultException.class, () -> em.createNamedQuery("oneByManyName").setParameter("name", "bad").getSingleResult());
    }

    @Test
    @DisplayName("change One for Many")
    void changeOneForMany(){
        One one = new One("one");
        prepareData(one);

        Many many = em.find(Many.class, one.getMany().get(0).getId());
        One one2 = new One("one2");
        one2.addMany(many);

        tx.begin();
        em.persist(one2);
        tx.commit();

        // the many should have the new one now
        assertEquals(one2, many.getOne());

        // the old one should have three manies now
        One oneRetr = em.find(One.class, one.getId());
        assertEquals(3, oneRetr.getMany().size());

        // the new one should have a single many
        One one2Retr = em.find(One.class, one2.getId());
        assertEquals(1, one2Retr.getMany().size());
    }


}
