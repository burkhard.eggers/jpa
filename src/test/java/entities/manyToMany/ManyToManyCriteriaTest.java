package entities.manyToMany;

import entities.BaseTest;
import jpatest.entities.manyToMany.Left;
import jpatest.entities.manyToMany.Left_;
import jpatest.entities.manyToMany.Right;
import jpatest.entities.manyToMany.Right_;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.criteria.*;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class ManyToManyCriteriaTest extends BaseTest {
    private Left left1;
    private Left left2;
    private Right right1;
    private Right right2;

    @Override
    protected void prepareEach() {

        // prepare 2 left and 2 right together
        tx.begin();
        //em.createQuery("delete from Right").executeUpdate();
        left1 = new Left("left1");
        left2 = new Left("left2");
        right1 = new Right("right1");
        right2 = new Right("right2");
        left1.addRight(right1).addRight(right2);
        right1.addLeft(left1).addLeft(left2);
        // no need to do because they are synchronized
        //left2.addRight(right1).addRight(right2);
        //right2.addLeft(left2).addLeft(left2);
        em.persist(left1);
        em.persist(left2);
        tx.commit();
    }

    @Test
    @DisplayName("all rights")
    void retrieveRights(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Right> query = cb.createQuery(Right.class);
        Root<Right> from = query.from(Right.class);
        List<Right> list = em.createQuery(query.select(from)).getResultList();
        assertEquals(2, list.size());
        assertEquals(list.get(0), right1);
        assertEquals(list.get(1), right2);
    }

    @Test
    @DisplayName("left per name")
    void retrieveLeftPerName(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Left> query = cb.createQuery(Left.class);
        Root<Left> from = query.from(Left.class);
        List<Left> list = em.createQuery(query.select(from).where(cb.equal(from.get(Left_.NAME), "left1"))).getResultList();
        assertEquals(1, list.size());
        assertEquals(list.get(0), left1);
    }

    @Test
    @DisplayName("left per right name")
    void retrieveLeftPerRightName(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Left> query = cb.createQuery(Left.class);
        Root<Left> from = query.from(Left.class);
        ListJoin<Left, Right> join = from.join(Left_.rights);
        List<Left> list = em.createQuery(
                query.select(from)
                        .where(cb.equal(join.get(Right_.NAME), right1.getName()))).getResultList();
        assertEquals(2, list.size());
        assertEquals(list.get(0), left1);

        // remove right1 from left1
        tx.begin();
        left1.removeRight(right1);
        em.persist(left1);
        tx.commit();
        list = em.createQuery(
                query.select(from)
                        .where(cb.equal(join.get(Right_.NAME), right1.getName()))).getResultList();
        // should only have left2 now
        assertEquals(1, list.size());
        assertEquals(list.get(0), left2);

    }

}
