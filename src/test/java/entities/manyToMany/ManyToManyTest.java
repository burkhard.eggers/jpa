package entities.manyToMany;

import entities.BaseTest;
import jpatest.entities.manyToMany.Left;
import jpatest.entities.manyToMany.Right;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class ManyToManyTest extends BaseTest {

    private Left left1;
    private Left left2;
    private Right right1;
    private Right right2;

    @Override
    protected void prepareEach() {

        // prepare 2 left and 2 right together
        tx.begin();
        //em.createQuery("delete from Right").executeUpdate();
        left1 = new Left("left1");
        left2 = new Left("left2");
        right1 = new Right("right1");
        right2 = new Right("right2");
        left1.addRight(right1).addRight(right2);
        right1.addLeft(left1).addLeft(left2);
        // no need to do because they are synchronized
        //left2.addRight(right1).addRight(right2);
        //right2.addLeft(left2).addLeft(left2);
        em.persist(left1);
        em.persist(left2);
        tx.commit();
    }

    @Test
    @DisplayName("check if there")
    void checkIfThere() {
        // there must be two of left/right
        int leftSize = em.createQuery("select l from M2MLeft l").getResultList().size();
        int rightSize = em.createQuery("select r from M2MRight r").getResultList().size();
        assertEquals(2, leftSize);
        assertEquals(2, rightSize);

        //retrieve and check
        Left l1 = em.find(Left.class, left1.getId());
        assertEquals(2, l1.getRights().size());
        assertEquals(l1, left1);

        // getRight method
        Optional<Right> optional = l1.getRight(right1.getId());
        assertEquals(true, optional.isPresent());
        assertEquals(right1, optional.get());

        // non existent Right
        optional = l1.getRight(567L);
        assertEquals(false, optional.isPresent());

        // right
        Right r1 = em.find(Right.class, right1.getId());
        assertEquals(2, r1.getLefts().size());
        assertEquals(r1, right1);
    }

    @Test
    @DisplayName("change name")
    void changeName() {
        //retrieve and check
        Left l1 = em.find(Left.class, left1.getId());
        tx.begin();
        l1.setName("new left1");
        em.persist(l1);
        tx.commit();

        // access it via right and see if the name is updated
        Right r1 = em.find(Right.class, right1.getId());
        assertEquals("new left1", r1.getLeft(left1.getId()).get().getName());
    }

    @Test
    @DisplayName("remove")
    void remove() {
        //remove a right
        Left l1 = em.find(Left.class, left1.getId());

        tx.begin();
        l1.removeRight(l1.getRights().get(0));
        em.persist(l1);
        tx.commit();

        //l1 and r1 should not have each other
        // l1 should have only right2
        Left l1Retr = em.find(Left.class, left1.getId());
        assertEquals(l1Retr, l1);
        assertEquals(1, l1.getRights().size());
        assertEquals("right2", l1.getRights().get(0).getName());

        // right 1 should have only left 2
        Right r1 = em.find(Right.class, right1.getId());
        assertEquals(1, r1.getLefts().size());
        assertEquals("left2", r1.getLefts().get(0).getName());
    }

    @Test
    @DisplayName("add")
    void add() {
        Left l1 = em.find(Left.class, left1.getId());

        tx.begin();
        l1.addRight(new Right("new right"));
        em.persist(l1);
        tx.commit();

        // right1 must have left 1 now
        Right rNew = (Right)em.createQuery("select r from M2MRight r where r.name = 'new right'").getSingleResult();

        Optional<Left> optional = rNew.getLeft(left1.getId());
        assertEquals(true, optional.isPresent());
        assertEquals(left1, optional.get());

        // left1 should have 3 right now
        assertEquals(3, em.find(Left.class, left1.getId()).getRights().size());
    }
}