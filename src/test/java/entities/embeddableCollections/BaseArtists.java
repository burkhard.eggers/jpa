package entities.embeddableCollections;


import jpatest.entities.embeddableCollections.Artist;
import jpatest.entities.embeddableCollections.Track;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BaseArtists {

     static Artist artist, artist2;

     static String JALA_JALA_TAG = "jala jala";
     static String RECONSTRUCCION_TAG = "reconstruccion";

     static String FANIA_TAG = "fania";

     static String RICHI_RAY_TAG = "richi ray";

     static String SONIDO_BESTIAL_TRACK_NAME = "sonido bestial";

     static String ATARA_LA_ARECHE_TRACK_NAME= "atara la areche";


     static void start(){
        artist = new Artist("Bobby", "Cruz")
                .addTrack(new Track("Salsa y control", 10))
                .setTags(List.of(FANIA_TAG, JALA_JALA_TAG, RECONSTRUCCION_TAG));
        artist2 = new Artist("Ricardo", "Ray")
                .addTrack(new Track(ATARA_LA_ARECHE_TRACK_NAME, 10))
                .addTrack((new Track(SONIDO_BESTIAL_TRACK_NAME, 5)))
                .setTags(List.of(RICHI_RAY_TAG, JALA_JALA_TAG, RECONSTRUCCION_TAG));
    }

}
