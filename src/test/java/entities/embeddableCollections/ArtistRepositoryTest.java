package entities.embeddableCollections;

import entities.BaseTest;
import jpatest.entities.embeddableCollections.Artist;
import jpatest.repositories.ArtistRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import static entities.embeddableCollections.BaseArtists.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ContextConfiguration(classes = {ArtistRepositoryTest.class})
@EnableJpaRepositories("jpatest.repositories")
@EntityScan({"jpatest.entities"})
public class ArtistRepositoryTest extends BaseTest {

    @Autowired
    private ArtistRepository repository;

    @PersistenceContext
    private EntityManager entityManager;

    protected void prepareEach() {
        prepare();
    }
    private void prepare(){
        // vor jedem test
        // 1. alles löschen
        // 2. neue Instanzen erzeugen
        // 3. instanzen persisitieren
        // 4. Instanzen aus context lösen

        // 1
        repository.deleteAll();
            repository.flush();
            // 2
            start();
            // 3
            // mit em geht es leider nicht.... :(
            //em.getTransaction().begin();
            //em.persist(artist);
            //em.persist(artist2);
            //em.setFlushMode(FlushModeType.COMMIT);
            //em.flush();
            //em.getTransaction().commit();
            repository.save(artist);
            repository.save(artist2);
            repository.flush();
            // 4
            //entityManager.detach(artist);
            //entityManager.detach(artist2);
    }
    @Test
    public void testSaveAndFind() {
        long count = repository.count();
        assertEquals(2L, count);
        Optional<Artist> byId = repository.findById(artist.getId());
        assertTrue(byId.isPresent());
        assertEquals(artist.toString(), byId.get().toString());
    }
    @Test
    public void testSaveAndFindByFirstName() {
        List<Artist> byFirstName = repository.findByFirstName(artist2.getFirstName());
        assertEquals(1, byFirstName.size());
        assertEquals(artist2.toString(), byFirstName.get(0).toString());

        byFirstName = repository.findByFirstNameQuery(artist2.getFirstName());
        assertEquals(1, byFirstName.size());
        assertEquals(artist2.toString(), byFirstName.get(0).toString());

    }

    @Test
    public void testSaveAndDelete() {
        Long count = repository.count();
        assertEquals(2L, count);
        repository.delete(artist2);
        count = repository.count();
        assertEquals(1L, count);
        repository.flush();
    }

    @Test
    @Commit
    public void testUpdate() {
        String NEW_FN = "%%% NewFirstName %%%";
        repository.setArtistFirstNameById(NEW_FN, artist.getId());

        entityManager.detach(artist);
        // must find no one with non existing first name
        List<Artist> retr = repository.findByFirstName("wrong name");
        assertEquals(0, retr.size());
        // must find one with new first name
        // by finder
        retr = repository.findByFirstName(NEW_FN);
        assertEquals(1, retr.size());
        // and by query
        retr = repository.findByFirstNameQuery(NEW_FN);
        assertEquals(1, retr.size());

        assertEquals(NEW_FN, retr.get(0).getFirstName());
    }
}
