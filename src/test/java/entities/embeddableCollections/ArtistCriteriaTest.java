package entities.embeddableCollections;


import entities.BaseTest;
import jpatest.entities.embeddableCollections.Artist;
import jpatest.entities.embeddableCollections.Artist_;
import jpatest.entities.embeddableCollections.Track;
import jpatest.entities.embeddableCollections.Track_;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static entities.embeddableCollections.BaseArtists.*;

public class ArtistCriteriaTest extends BaseTest {

    protected void prepareEach(){
        em.getTransaction().begin();
        em.createQuery("delete from Artist").executeUpdate();
        start();
        em.persist(artist);
        em.persist(artist2);
        em.getTransaction().commit();
        // clears all persistent context
        em.clear();
    }


    @DisplayName("retrieve all artist with criteria")
    @Test
    void retrieveArtistsWithCriteria(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Artist> artistQuery = cb.createQuery(Artist.class);
        List<Artist> allArtists = em.createQuery(
                artistQuery.select(artistQuery.from(Artist.class)))
                .getResultList();
        assertEquals(2, allArtists.size());

        assertEquals(allArtists.get(0).toString(), artist.toString());
        assertEquals(allArtists.get(1).toString(), artist2.toString());
    }

    @DisplayName("retrieve artist by id")
    @Test
    void retrieveArtistWithId(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Artist> artistQuery = cb.createQuery(Artist.class);
        Root<Artist> from = artistQuery.from(Artist.class);
        artistQuery.select(from)
                .where(cb.equal(from.get(Artist_.id), artist.getId()));
        List<Artist> artistsWithId = em.createQuery(artistQuery).getResultList();
        assertEquals(1, artistsWithId.size());

        assertEquals(artistsWithId.get(0).toString(), artist.toString());
    }

    @DisplayName("retrieve artist by track duration")
    @Test
    void retrieveArtistWithTrackDuration(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Artist> artistQuery = cb.createQuery(Artist.class);
        Root<Artist> from = artistQuery.from(Artist.class);
        ListJoin<Artist, Track> join = from.join(Artist_.tracks, JoinType.INNER);
        // artists with track with duration 10
        artistQuery.select(from)
                .where(cb.equal(join.get(Track_.duration), 10));
        List<Artist> artistsWithTrackDuration10 = em.createQuery(artistQuery).getResultList();
        assertEquals(2, artistsWithTrackDuration10.size());

        // artists with track with duration 5
        artistQuery.select(from)
                .where(cb.equal(join.get(Track_.duration), 5));
        List<Artist> artistsWithTrackDuration5 = em.createQuery(artistQuery).getResultList();
        assertEquals(1, artistsWithTrackDuration5.size());
    }

    @DisplayName("retrieve artist by tag")
    @Test
    void retrieveArtistByTag(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Artist> artistQuery = cb.createQuery(Artist.class);
        Root<Artist> from = artistQuery.from(Artist.class);
        ListJoin<Artist, String> join = from.join(Artist_.tags);

        // artists with 'jala jala' tag
        artistQuery.select(from)
                .where(join.in( JALA_JALA_TAG));
        List<Artist> artistsWithJalaJalaTag = em.createQuery(artistQuery).getResultList();
        assertEquals(2, artistsWithJalaJalaTag.size());

        // artists with 'richi ray' tag
        artistQuery.select(from)
                .where(join.in( RICHI_RAY_TAG));
        List<Artist> artistsWithRRTag = em.createQuery(artistQuery).getResultList();
        assertEquals(1, artistsWithRRTag.size());
    }

    @DisplayName("retrieve artist by tag and track name")
    @Test
    void retrieveArtistByTagAndTrackName(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Artist> artistQuery = cb.createQuery(Artist.class);
        Root<Artist> from = artistQuery.from(Artist.class);
        ListJoin<Artist, String> tagsJoin = from.join(Artist_.tags);
        ListJoin<Artist, Track> tracksJoin = from.join(Artist_.tracks);

        artistQuery.select(from)
                .where(
                        cb.and(
                            tagsJoin.in( RICHI_RAY_TAG),
                            cb.equal(tracksJoin.get(Track_.name), SONIDO_BESTIAL_TRACK_NAME)
                ));
        // there is exactly one artist with sonido bestial track and fania tag
        List<Artist> artistsWithRRTagAndSonidoBestial = em.createQuery(artistQuery).getResultList();
        assertEquals(1, artistsWithRRTagAndSonidoBestial.size());

        artistQuery.select(from)
                .where(
                        cb.and(
                                tagsJoin.in( FANIA_TAG),
                                cb.equal(tracksJoin.get(Track_.name), ATARA_LA_ARECHE_TRACK_NAME)
                        ));
        // there is no artist with atara track and fania tag
        List<Artist> artistsWithFaniaTagAndAtara = em.createQuery(artistQuery).getResultList();
        assertTrue(artistsWithFaniaTagAndAtara.isEmpty());
    }

    @DisplayName("update artist by tag")
    @Test
    void updateArtistByFirstName(){
        String NEW_FN = "newFirstName";
        String NEW_LN = "newLastName";
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaUpdate<Artist> artistUpdate = cb.createCriteriaUpdate(Artist.class);
        Root<Artist> from = artistUpdate.from(Artist.class);
        em.getTransaction().begin();
        artistUpdate
                .set(Artist_.firstName, NEW_FN)
                .set(Artist_.lastName, NEW_LN)
                .where(cb.equal(from.get(Artist_.firstName), artist2.getFirstName()));
        int nr = em.createQuery(artistUpdate).executeUpdate();
        em.getTransaction().commit();
        // one should have been updated
        assertEquals(1, nr);

        // retrieve all with new full name
        CriteriaQuery<Artist> artistQuery = cb.createQuery(Artist.class);
        Root<Artist> selectFrom = artistQuery.from(Artist.class);
        artistQuery.select(from)
                .where(cb.and(
                        cb.equal(selectFrom.get(Artist_.firstName), NEW_FN),
                        cb.equal(selectFrom.get(Artist_.lastName), NEW_LN)
                ));
        List<Artist> artists  = em.createQuery(artistQuery).getResultList();
        // should be one with new names
        assertEquals(1, artists.size());
        artists.forEach((a) -> {
            assertEquals(NEW_FN + " " + NEW_LN , a.fullname());
        });
    }

    @DisplayName("update artist name by tag ans track")
    @Test
    void updateArtistNameByTagAndTrack(){
        String NEW_FN = "newFirstName";
        String NEW_LN = "newLastName";
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaUpdate<Artist> artistUpdate = cb.createCriteriaUpdate(Artist.class);
        Root<Artist> from = artistUpdate.from(Artist.class);
        Subquery<Artist> subquery = artistUpdate.subquery(Artist.class);
        Root<Artist> subFrom = subquery.from(Artist.class);
        ListJoin<Artist, String> tagsJoin = subFrom.join(Artist_.tags);
        ListJoin<Artist, Track> tracksJoin = subFrom.join(Artist_.tracks);

        subquery.select(subFrom)
                .where(
                        cb.and(
                                cb.equal(subFrom.get(Artist_.id), from.get(Artist_.id)),
                                tagsJoin.in( RICHI_RAY_TAG),
                                cb.equal(tracksJoin.get(Track_.name), SONIDO_BESTIAL_TRACK_NAME)
                        ));

        em.getTransaction().begin();
        artistUpdate
                .set(Artist_.firstName, NEW_FN)
                .set(Artist_.lastName, NEW_LN)
                .where(cb.exists(subquery));
        int nr = em.createQuery(artistUpdate).executeUpdate();
        em.getTransaction().commit();
        // 1 artist should have been renamed
        assertEquals(1, nr);

        //check
        // artist2 (ricardo ray) should be the renamed one and it must have the new name now
        Artist renamed = em.find(Artist.class, artist2.getId());
        assertEquals(NEW_FN + " " + NEW_LN, renamed.fullname());

        // there must be single artist with new name
        Query query = em.createQuery("SELECT a from Artist a WHERE a.firstName = :fn AND a.lastName = :ln");
        query.setParameter("fn", NEW_FN);
        query.setParameter("ln", NEW_LN);
        List<Artist> resultList = query.getResultList();
        assertEquals(1, resultList.size());
        assertEquals(NEW_FN + " " + NEW_LN, resultList.get(0).fullname());
    }

}
