package entities.embeddableCollections;


import entities.BaseTest;
import jpatest.entities.embeddableCollections.Artist;
import jpatest.entities.embeddableCollections.Track;
import org.junit.jupiter.api.*;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ArtistTest extends BaseTest {

    protected void prepareEach(){
        tx.begin();
        em.createQuery("delete from Artist").executeUpdate();
        tx.commit();
    }

    @DisplayName("retrieve non existent must give null")
    @Test
    void retrieveNonExistent(){
        Artist artistRetrieved = em.find(Artist.class, 123L);
        assertNull(artistRetrieved);
    }

    @DisplayName("rollback")
    @Test
    void rollback(){
        Artist artist = new Artist("Adams","Douglas");
        tx.begin();
        em.persist(artist);
        tx.rollback();
        Long id = artist.getId();
        Artist artistRetrieved = em.find(Artist.class, id);

        assertNull(artistRetrieved);
    }

    @DisplayName("add and retrieve artist")
    @Test
    void addAndRetrieve(){
        Artist artist = new Artist("Doug", "Adams")
                .addTrack(new Track("track1", 10))
                .setTags(List.of("don't", "panic", "dontyou!"));
        assertNull(artist.getId());
        tx.begin();
        em.persist(artist);
        assertNotNull(artist.getId());
        tx.commit();
        Long id = artist.getId();
        assertNotNull(id);
        Artist artistRetrieved = em.find(Artist.class, id);
        assertEquals("Doug Adams", artistRetrieved.fullname());
        assertEquals(artistRetrieved.getTracks().size(), 1);
        assertEquals(artistRetrieved.getTags().size(), 3);
        assertEquals(artist, artistRetrieved);
    }

    @DisplayName("change track")
    @Test
    void addTrackAndIncreaseAllDurationsBy10(){
        Artist artist = new Artist("Doug", "Adams")
                .addTrack(new Track("panic", 200))
                .addTrack(new Track("panicking", 20));
        // add some artist with tracks
        tx.begin();
        em.persist(artist);
        tx.commit();

        // add a track, remove another and increase duration of all tracks by 10
        tx.begin();
        artist.addTrack(new Track("dontyou", 20));
        List<Track> tracks = artist.getTracks();
        tracks.forEach((track) -> track.setDuration(track.getDuration() + 10));
        artist.removeTrack(1);
        em.persist(artist);
        tx.commit();

        Artist a1 = em.find(Artist.class, artist.getId());
        assertEquals(a1.getTracks().get(1).getDuration(), 30);
        assertEquals(a1.getTracks().get(0).getDuration(), 210);
        assertEquals(a1.getTracks().size(), 2);
    }

    @DisplayName("remove and add tag")
    @Test
    void removeAndAddTag() {
        Artist artist = new Artist("Doug", "Adams")
                .addTag("t1")
                .addTag("t2")
                .addTag("t3");
        // add some artist with tracks
        tx.begin();
        em.persist(artist);
        tx.commit();

        Artist artist1 = em.find(Artist.class, artist.getId());
        tx.begin();
        artist1
                .removeTag("t2")
                .addTag("t4");
        tx.commit();

        Artist artist2 = em.find(Artist.class, artist.getId());
        assertEquals(new ArrayList<>(artist2.getTags()), List.of("t1", "t3", "t4"));
    }

    private void prepareForQueries( Track track){
        tx.begin();
        em.persist(new Artist("Doug", "Adams")
                .addTag("t1")
                .addTag("t2")
                .addTag("t3")
                .addTrack(new Track("tr1", 200))
                .addTrack(track)
        );
        em.persist(new Artist("Doug2", "Adams")
                .addTag("t4")
                .addTag("t2")
                .addTag("t5")
                .addTrack(track)
        );
        tx.commit();
    }

    @DisplayName("all Doug")
    @Test
    void perFirstName() {
        prepareForQueries(new Track("tr2", 400));

        // retrieve all with Doug first name
        List<Artist> list = (List<Artist>) em.createQuery("select a from Artist a where a.firstName LIKE 'Doug%'").getResultList();
        assertEquals(list.size(), 2);
    }
    @Test
    @DisplayName("all with t2 tag")
    void AllWithTag() {
        prepareForQueries(new Track("tr2", 400));
        // retrieve all with t2 tag
        Query fromTag =
                em.createQuery("select a from Artist a " +
                        "inner join a.tags t where t = :tag");
        assertEquals(2, fromTag
                .setParameter("tag", "t2")
                .getResultList().size());
    }

    @Test
    @DisplayName("all with tr2 track")
    void AllWithTrack(){
        Track track = new Track("tr2", 400);
        prepareForQueries(track);

        Query fromTrack =
                em.createQuery(
                        "select a from Artist a " +
                                "join a.tracks track " +
                                "where track.name = :name " +
                                "and track.duration = :duration");
        int track2Size = fromTrack
                .setParameter("name", track.getName())
                .setParameter("duration", track.getDuration())
                .getResultList().size();
        assertEquals(2, track2Size);
    }

    @DisplayName("all with track and tag")
    @Test
    void queries() {
        Track track = new Track("tr2", 400);
        prepareForQueries(track);
        Query fromTrackAndTag =
                em.createQuery(
                        "select a from Artist a " +
                                "join a.tracks track " +
                                "join a.tags tag " +
                                "where tag = :tag " +
                                "and track.name = :name " +
                                "and track.duration = :duration"
                );
        // should be 2 with tag t2 and track tr2
        assertEquals(2, fromTrackAndTag
                .setParameter("tag", "t2")
                .setParameter("name", track.getName())
                .setParameter("duration", track.getDuration())
                .getResultList().size());

        // should be 1 with tag t5 and track tr2
        assertEquals(1, fromTrackAndTag
                .setParameter("tag", "t5")
                .setParameter("name", track.getName())
                .setParameter("duration", track.getDuration())
                .getResultList().size());
    }

    @DisplayName("named query")
    @Test
    void namedQuery(){
        prepareForQueries(new Track("tr2", 400));
        int adamsSize = em
                .createNamedQuery("findFromLastName")
                .setParameter("last", "Adams")
                .getResultList()
                .size();
        assertEquals(2, adamsSize);
    }

    @DisplayName("update name of artist with tag ans track name")
    @Test
    void updateNameOfArtistWithTagAndTrackName(){
        String TRACK_NAME = "tr2";
        prepareForQueries(new Track(TRACK_NAME, 400));
        Query query = em.createQuery(
                "UPDATE Artist a " +
                        "SET a.firstName = 'Anders Fogh', a.lastName = 'Rasmussen' " +
                        "WHERE EXISTS (" +
                        "select a1 from Artist a1 " +
                        "join a1.tags tag " +
                        "join a1.tracks track " +
                        "where a1.id = a.id AND tag = :tag AND track.name = :track" +
                        ")"

        );
        // no artist should be updated with bullshit track name
        query.setParameter("tag", "t1");
        query.setParameter("track", "oijoijiojoij");
        em.getTransaction().begin();
        int i = query.executeUpdate();
        em.getTransaction().commit();
        assertEquals(0, i);

        // artist should be updated with existing track name
        query.setParameter("tag", "t1");
        query.setParameter("track", TRACK_NAME);
        em.getTransaction().begin();
        i = query.executeUpdate();
        em.getTransaction().commit();
        assertEquals(1, i);

    }


}
