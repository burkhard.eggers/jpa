package entities.oneToOne;

import entities.BaseTest;
import jpatest.entities.oneToOne.Address;
import jpatest.entities.oneToOne.Customer;
import org.junit.jupiter.api.*;

import javax.persistence.*;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerTest extends BaseTest {

    protected void prepareEach(){
        tx.begin();
        em.createQuery("delete from O2OCustomer").executeUpdate();
        em.createQuery("delete from O2OAddress").executeUpdate();
        tx.commit();
    }

    @Test
    @DisplayName("create customer without address")
    void createCustomerWithoutAddress(){
        Customer customer = new Customer("kunde 1");
        tx.begin();
        em.persist(customer);
        tx.commit();

        Customer customerRetrieved = em.find(Customer.class, customer.getId());

        assertEquals(customerRetrieved, customer);
        assertNull(customerRetrieved.getAddress());
        int size = em.createQuery("select c from O2OCustomer c").getResultList().size();
        assertEquals(1, size);
    }

    @Test
    @DisplayName("create customer with address")
    void createCustomerWithAddressAndRetrieve(){
        Customer customer = new Customer("kunde 1");
        Address address = new Address("plz");
        customer.setAddress(address);
        tx.begin();
        em.persist(customer);
        em.persist(address);
        tx.commit();

        Customer customerRetrieved = em.find(Customer.class, customer.getId());

        assertNotNull(customerRetrieved.getAddress().getName());
        assertEquals(customerRetrieved, customer);
        int size = em.createQuery("select c from O2OCustomer c").getResultList().size();
        assertEquals(1, size);
    }

    @Test
    @DisplayName("create two customers with same address")
    void createTwoCustomersWithSameAddress(){
        // create address
        Address address = new Address("plz2");
        tx.begin();
        em.persist(address);
        tx.commit();

        // create two customers with same address
        Customer customer1 = new Customer("kunde 2");
        Customer customer2 = new Customer("kunde 3");
        customer1.setAddress(address);
        customer2.setAddress(address);
        tx.begin();
        em.persist(customer1);
        em.persist(customer2);
        tx.commit();

        Customer customer1Retr = em.find(Customer.class, customer1.getId());
        Customer customer2Retr = em.find(Customer.class, customer2.getId());

        int size = em.createQuery("select c from O2OCustomer c where c.address.name = 'plz2'").getResultList().size();
        assertEquals(2, size);
        assertEquals(customer1Retr, customer1);
        assertEquals(customer2Retr, customer2);

        int sizeAdr = em.createQuery("select a from O2OAddress a").getResultList().size();
        assertEquals(1, sizeAdr);
    }

    @Test
    @DisplayName("change address of customer")
    void changeAddressOfCustomer(){
        // create address
        Address address1 = new Address("plz");
        tx.begin();
        em.persist(address1);
        tx.commit();

        // create customer with address1
        Customer customer = new Customer("kunde");
        customer.setAddress(address1);
        tx.begin();
        em.persist(customer);
        tx.commit();

        Query query = em.createQuery("select c.address.name from O2OCustomer c where c.id = :id");

        String addr = (String)query
                .setParameter("id", customer.getId())
                .getSingleResult();
        assertEquals(address1.getName(), addr);

        // change name of customer's address
        address1.setName("name2");
        tx.begin();
        em.persist(address1);
        tx.commit();

        addr = (String)query
                .setParameter("id", customer.getId())
                .getSingleResult();
        assertEquals(address1.getName(), addr);

        // change address object of customer

        Address address2 = new Address("name of new address");
        tx.begin();
        em.persist(address2);
        tx.commit();
        customer.setAddress(address2);
        tx.begin();
        em.persist(customer);
        tx.commit();

        addr = (String)query
                .setParameter("id", customer.getId())
                .getSingleResult();
        assertEquals(address2.getName(), addr);
    }

    @Test
    @DisplayName("remove customer")
    void removeCustomer(){
        Customer customer = new Customer("kunde 1");
        Address address = new Address("plz");
        customer.setAddress(address);
        tx.begin();
        em.persist(customer);
        tx.commit();

        assertEquals(customer, em.find(Customer.class, customer.getId()));
        assertEquals(address, em.find(Address.class, address.getId()));

        // remove
        tx.begin();
        em.remove(customer);
        tx.commit();

        assertNull(em.find(Customer.class, customer.getId()));
        assertNull(em.find(Address.class, address.getId()));

    }

}
