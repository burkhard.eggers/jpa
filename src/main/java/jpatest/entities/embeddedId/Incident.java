package jpatest.entities.embeddedId;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Incident {
    public Incident(){

    }
    @EmbeddedId
    @NonNull
    private IncidentId id;

    @NonNull
    private String what;
}
