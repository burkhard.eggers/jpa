package jpatest.entities.embeddedId;

public enum IncidentType {
    FATAL,
    SEVERE,
    NORMAL
}
