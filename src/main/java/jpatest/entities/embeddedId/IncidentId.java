package jpatest.entities.embeddedId;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Embeddable
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class IncidentId implements Serializable {

    public IncidentId(){

    }

    @NonNull
    @Enumerated(EnumType.STRING)
    private IncidentType type;

    @NonNull
    private LocalDate date;
}
