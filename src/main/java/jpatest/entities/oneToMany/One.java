package jpatest.entities.oneToMany;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
@NamedQuery(
        name = "oneByManyName",
        query = "select o from One o where exists (select m from Many m where m.name = :name)"
)
public class One {

    @Id
    @GeneratedValue()
    private Long id;

    @NonNull
    private String name;

    @OneToMany(mappedBy = "one", cascade = CascadeType.ALL)
    private List<Many> many = new ArrayList<>();

    public One(){

    }
    public One addMany(Many m){
        if(!this.many.contains(m)) {
            this.many.add(m);
            m.setOne(this);
        }
        return this;
    }

    public One removeMany(Many mn){
        if(this.many.contains(mn)) {
            this.many.remove(mn);
        }
        return this;
    }
}
