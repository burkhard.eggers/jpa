package jpatest.entities.oneToMany;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Many {

    @Id
    @GeneratedValue()
    private Long id;

    public Many(){

    }
    @NonNull
    String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private One one;
}
