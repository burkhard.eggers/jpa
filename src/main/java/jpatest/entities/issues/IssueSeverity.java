package jpatest.entities.issues;

public enum IssueSeverity {
    HIGH,
    NORMAL,
    LOW
}
