package jpatest.entities.issues;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class IssuesDAO {
    @NonNull
    private EntityManager em;

    public List<Issue> issuesFromUserAndSeverity(Long userId, Status ... st){
        List<Status> list = List.of(st);
        return em.createQuery("select i from Issue i " +
                " join i.userAssignments user " +
                "where user.id = :userId and i.status in :stati")
                .setParameter("userId", userId)
                .setParameter("stati", list)
                .getResultList();
    }

    private void doAssign(boolean assign, Long userId, long issueId ){
        EntityTransaction tr = em.getTransaction();
        tr.begin();
        User user = em.find(User.class, userId);
        Issue i = em.find(Issue.class, issueId);
        if(assign) i.assign(user);
        else i.unassign(user);
        em.persist(i);
        em.persist(user);
        tr.commit();
    }

    public List<Task> unResolvedTasksForUsersIssues(long userId){
        return em.createQuery("select t from Task t join t.issue.userAssignments user " +
        "where user.id = :userId " +
                        "and t.resolvedByUser = null"
                )
                .setParameter("userId", userId)
                .getResultList();
    }

    public void assign(Long userId, long issueId ){
        doAssign(true, userId, issueId);
    }

    public void unassign(Long userId, long issueId ){
        doAssign(false, userId, issueId);
    }

    private boolean checkResolved(TaskContainer t){
        boolean ret = true;
        for (Task s : t.getSubTasks()) {
            if(s.getResolvedByUser() == null && !checkResolved(s)) ret = false;
        }
        return ret;
    }

    public void resolveTask(Long userId, long taskId ){
        EntityTransaction tr = em.getTransaction();
        tr.begin();
        Task t = em.find(Task.class, taskId);
        t.setResolvedByUser(userId);
        em.persist(t);
        if(checkResolved(t.getIssue())) {
            t.getIssue().setStatus(Status.RESOLVED);
            em.persist(t.getIssue());
        }
        tr.commit();
    }

    public void unresolveTask(long taskId ){
        EntityTransaction tr = em.getTransaction();
        tr.begin();
        Task t = em.find(Task.class, taskId);
        t.setResolvedByUser(null);
        em.persist(t);
        if(checkResolved(t.getIssue())) {
            t.getIssue().setStatus(Status.ASSIGNED);
            em.persist(t.getIssue());
        }
        tr.commit();
    }

}
