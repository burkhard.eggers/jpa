package jpatest.entities.issues;

import jpatest.entities.manyToMany.Left;
import jpatest.entities.manyToMany.Right;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "entityUser")
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class User {
    public User(){

    }
    @Id
    @GeneratedValue()
    private Long id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    public String fullName(){
        return firstName + " " + lastName;
    }

    @ManyToMany
    private List<Issue> assignments = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public User setAddress(Address a){
        if(!a.equals(address)) {
            address = a;
            a.setUser(this);
        }
        return this;
    }

    public User assign(Issue i){
        if(!this.assignments.contains(i)){
            this.assignments.add(i);
            i.assign(this);
        }
        return this;
    }

    public User unassign(Issue i) {
        if (this.assignments.contains(i)) {
            this.assignments.remove(i);
            i.unassign(this);
        }
        return this;
    }
}
