package jpatest.entities.issues;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Street {
    public Street(){

    }
    @Id
    @GeneratedValue()
    private Long id;

    @NonNull
    private String name;

    @ManyToOne
    private City city;


}
