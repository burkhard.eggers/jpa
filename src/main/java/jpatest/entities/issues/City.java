package jpatest.entities.issues;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class City {
    public City(){

    }
    @Id
    @GeneratedValue()
    private Long id;

    @NonNull
    private String name;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL)
    private List<Street> streets = new ArrayList<>();

    public City addStreet(Street s){
        if(!streets.contains(s)) {
            streets.add(s);
            s.setCity(this);
        }
        return this;
    }

    public City removeStreet(Street s){
        if(streets.contains(s)) {
            streets.remove(s);
            s.setCity(null);
        }
        return this;
    }
}
