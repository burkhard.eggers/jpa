package jpatest.entities.issues;

public enum Status {
    UNASSIGNED,
    ASSIGNED,
    RESOLVED
}
