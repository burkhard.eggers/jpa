package jpatest.entities.issues;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TaskContainer {

    @Id
    @GeneratedValue()
    private Long id;

    public TaskContainer(){

    }

    @NonNull
    private String title;

    @ManyToOne
    protected TaskContainer container;

    @OneToMany(mappedBy = "container", cascade = CascadeType.ALL)
    protected List<Task> subTasks = new ArrayList<>();

    public TaskContainer addSubTask(Task t){
        if(!this.subTasks.contains(t)) {
            this.subTasks.add(t);
        }
        return this;
    }

    public TaskContainer removeSubTask(Task t){
        if(this.subTasks.contains(t)) {
            this.subTasks.remove(t);
        }
        return this;
    }
}
