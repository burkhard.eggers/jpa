package jpatest.entities.issues;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Address {
    public Address(){

    }
    @Id
    @GeneratedValue()
    private Long id;

    @NonNull
    private Integer number;

    @OneToOne(cascade = CascadeType.ALL)
    private Street street;

    public Address setUser(User u){
        if(!u.equals(user)){
            user = u;
            u.setAddress(this);
        }
        return this;
    }

    @OneToOne(cascade = CascadeType.ALL)
    private User user;
}
