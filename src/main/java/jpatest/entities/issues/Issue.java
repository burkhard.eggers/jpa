package jpatest.entities.issues;

import jpatest.entities.manyToMany.Right;
import jpatest.log.LoggingListener;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Issue extends TaskContainer{

    @Id
    @GeneratedValue()
    private Long id;

    public Issue(String title, IssueSeverity severity){
        super(title);
        setSeverity(severity);
    }

    @Enumerated(EnumType.STRING)
    private Status status = Status.UNASSIGNED;

    private String content;

    @NonNull
    @Enumerated(EnumType.STRING)
    IssueSeverity severity;

    @ManyToMany(mappedBy = "assignments", cascade = CascadeType.ALL)
    private List<User> userAssignments = new ArrayList<>();

    @PrePersist
    void prePersist(){
        getSubTasks().forEach((s) -> {
            s.setIssue(this);
        });
        if(status != Status.RESOLVED) status =
                userAssignments.size() > 0 ? Status.ASSIGNED : Status.UNASSIGNED;
    }
    public Issue assign(User user) {
        if(!this.userAssignments.contains(user)){
            this.userAssignments.add(user);
            user.assign(this);
        }
        return this;
    }

    public Issue unassign(User user) {
        if(this.userAssignments.contains(user)){
            this.userAssignments.remove(user);
            user.unassign(this);
        }
        return this;
    }

}
