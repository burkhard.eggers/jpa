package jpatest.entities.issues;

import jpatest.entities.oneToMany.Many;
import jpatest.entities.oneToMany.One;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
public class Task extends TaskContainer{

    @Id
    @GeneratedValue()
    private Long id;

    public Task(){

    }

    public Task(String title){
        super(title);
    }

    @OneToOne
    private Issue issue;

    private Long resolvedByUser;

    @PrePersist
    void prePersist(){
        if(container != null && container instanceof Task) {
            setIssue(((Task) container).getIssue());
        }
        getSubTasks().forEach((s) -> {
            s.setIssue(getIssue());
            s.setContainer(this);
        });
    }
}
