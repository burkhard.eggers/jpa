package jpatest.entities.oneToOne;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity(name = "O2OAddress")
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Address {

    public Address(){

    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @NonNull
    @Column
    private String name;

    @OneToOne
    private Customer customer;
}
