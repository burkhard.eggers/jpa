package jpatest.entities.oneToOne;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity(name = "O2OCustomer")
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Customer {

    public Customer(){

    }
    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String name;

    @JoinColumn(name="address")
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

}
