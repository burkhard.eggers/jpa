package jpatest.entities.manyToMany;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity(name = "M2MLeft")
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Left {

    @Id
    @GeneratedValue()
    private Long id;

    public Left(){

    }
    @NonNull
    String name;

    @ManyToMany(mappedBy = "lefts", cascade = CascadeType.ALL)
    private List<Right> rights = new ArrayList<>();

    public Optional<Right> getRight(Long id){
        return this.rights.stream().filter((r) -> r.getId().equals(id)).findFirst();
    }

    public Left addRight(Right r){
        if(!this.rights.contains(r)){
            this.rights.add(r);
            r.addLeft(this);
        }
        return this;
    }

    public Left removeRight(Right r){
        if(this.rights.contains(r)){
            this.rights.remove(r);
            r.removeLeft(this);
        }
        return this;
    }

}
