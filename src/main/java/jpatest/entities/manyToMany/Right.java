package jpatest.entities.manyToMany;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity(name = "M2MRight")
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
public class Right {

    @Id
    @GeneratedValue()
    private Long id;

    public Right(){

    }
    @NonNull
    String name;

    @JoinTable(
            name = "left_right",
            joinColumns = @JoinColumn(name = "rightfk"),
            inverseJoinColumns = @JoinColumn(name = "leftfk")
    )
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Left> lefts = new ArrayList<>();

    public Optional<Left> getLeft(Long id){
        return this.lefts.stream().filter((l) -> l.getId().equals(id)).findFirst();
    }

    public Right addLeft(Left l){
        if(!this.lefts.contains(l)){
            this.lefts.add(l);
            l.addRight(this);
        }
        return this;
    }

    public Right removeLeft(Left l){
        if(this.lefts.contains(l)) {
            this.lefts.remove(l);
            l.removeRight(this);
        }
        return this;
    }
}
