package jpatest.entities.embeddableCollections;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
@Accessors(chain = true)
@RequiredArgsConstructor
@ToString
public class Track {
    public Track(){}

    @NonNull
    private String name;

    @NonNull
    private Integer duration;
}
