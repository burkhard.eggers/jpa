package jpatest.entities.embeddableCollections;

import lombok.*;
import lombok.experimental.Accessors;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Accessors(chain = true)
@RequiredArgsConstructor
@ToString
@NamedQuery(
    name="findFromLastName",
    query="select a from Artist a where a.lastName = :last"
)
public class Artist {

        public Artist(){

        }

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        @Column(nullable = false)
        @NonNull
        private String firstName;

        @Column(nullable = false)
        @NonNull
        private String lastName;

        private String email;
        private LocalDate dateOfBirth;

        public String fullname(){
            return firstName + " " + lastName;
        }

        @ElementCollection(fetch = FetchType.LAZY)
        private List<String> tags;

        @ElementCollection(fetch = FetchType.LAZY)
        private List<Track> tracks;

        public Artist addTag(String neu) {
            tags = tags != null ? new ArrayList<>(tags) : new ArrayList<>();
            tags.add(neu);
            return this;
        }

        public Artist removeTag(String tag) {
            tags = tags != null ? new ArrayList<>(tags) : new ArrayList<>();
            tags.remove(tag);
            return this;
        }

        public Artist addTrack(Track track) {
            tracks = tracks != null ? new ArrayList<>(tracks) : new ArrayList<>();
            tracks.add(track);
            return this;
        }

        public Artist removeTrack(int nr){
            tracks.remove(nr);
            return this;
        }
}
