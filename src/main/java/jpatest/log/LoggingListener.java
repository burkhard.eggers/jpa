package jpatest.log;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;

public class LoggingListener {

    @PostLoad
    @PrePersist
    public void onPostLoad(Object entity) {
        System.out.println("loaded" + entity);
    }
}
