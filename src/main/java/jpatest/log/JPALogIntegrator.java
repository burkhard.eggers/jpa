package jpatest.log;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.*;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

public class JPALogIntegrator implements Integrator {

    @Override
    public void integrate(Metadata metadata, SessionFactoryImplementor sessionFactory, SessionFactoryServiceRegistry serviceRegistry) {
        EventListenerRegistry eventListenerRegistry = serviceRegistry.getService(EventListenerRegistry.class);

        eventListenerRegistry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(new PreUpdateEventListener() {
            @Override
            public boolean onPreUpdate(PreUpdateEvent event) {
                return false;
            }
        });
        eventListenerRegistry.getEventListenerGroup(EventType.PRE_LOAD).appendListener(new PreLoadEventListener() {
            @Override
            public void onPreLoad(PreLoadEvent event) {
                System.out.println("pre load");
            }
        });
        //eventListenerRegistry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(new MySecurityHibernateEventListener());
        eventListenerRegistry.getEventListenerGroup(EventType.POST_LOAD).appendListener(new PostLoadEventListener() {
            @Override
            public void onPostLoad(PostLoadEvent event) {
                System.out.println("post load");
            }
        });
    }

    @Override
    public void disintegrate(SessionFactoryImplementor sessionFactory, SessionFactoryServiceRegistry serviceRegistry) {

    }
}
