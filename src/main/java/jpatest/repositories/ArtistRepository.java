package jpatest.repositories;

import jpatest.entities.embeddableCollections.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long> {

    List<Artist> findByFirstName(String firstName);

    @Query("select a from Artist a where a.firstName = ?1")
    List<Artist> findByFirstNameQuery(String firstName);


    @Modifying
    @Query("update Artist a set a.firstName = ?1 where a.id = ?2")
    void setArtistFirstNameById(String firstname, Long Id);
}
